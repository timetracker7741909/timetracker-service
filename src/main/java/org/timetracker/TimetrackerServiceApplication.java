package org.timetracker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.retry.annotation.EnableRetry;

@EnableRetry
@SpringBootApplication
public class TimetrackerServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(TimetrackerServiceApplication.class, args);
	}
}
