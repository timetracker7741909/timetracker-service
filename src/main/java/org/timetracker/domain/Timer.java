package org.timetracker.domain;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.OrderBy;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class Timer {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String title;

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "current_run_id")
	private TimerRecord currentRun;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@OrderBy("startedTime DESC")
	@JoinColumn(name = "timer_id")
	private List<TimerRecord> records;

	public Timer(String title) {
		this.title = title;
		this.records = new ArrayList<>();
	}

	public boolean isRunning() {
		return null != currentRun && !currentRun.isCompleted();
	}

	public Instant getStartedTime() {
		return null != currentRun ? currentRun.getStartedTime() : null;
	}

	public Instant getStoppedTime() {
		return null != currentRun ? currentRun.getStoppedTime() : null;
	}

	public Duration getDuration() {
		return records.stream()
			.filter(TimerRecord::isCompleted)
			.map(TimerRecord::getDuration)
			.reduce(Duration.ZERO, (d1, d2) -> d1.plus(d2));
	}

	public void start(Instant time) {
		if(isRunning())
			throw new IllegalStateException(String.format("Timer #%d is already running", id));

		this.currentRun = new TimerRecord(time);
		records.add(currentRun);
	}

	public void stop(Instant time) {
		if(!isRunning())
			throw new IllegalStateException(String.format("Timer #%d is not running", id));

		this.currentRun.complete(time);
	}
}
