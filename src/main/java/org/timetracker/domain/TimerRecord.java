package org.timetracker.domain;

import java.time.Duration;
import java.time.Instant;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class TimerRecord {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private Instant startedTime;
	private Instant stoppedTime;

	public TimerRecord(Instant startedTime) {
		this.startedTime = startedTime;
	}

	public boolean isCompleted() {
		return null != stoppedTime;
	}

	public Duration getDuration() {
		if(!isCompleted())
			throw new IllegalStateException(String.format("Record #%d is not completed", id));

		return Duration.between(startedTime, stoppedTime);
	}

	public void complete(Instant time) {
		this.stoppedTime = time;
	}

	@Override
	public String toString() {
		return String.format("Record[#%s, %s, %s]", id, startedTime, stoppedTime);
	}
}
