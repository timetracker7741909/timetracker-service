package org.timetracker.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.timetracker.domain.Timer;

@Repository
public interface TimerRepository extends JpaRepository<Timer, Long> {}
