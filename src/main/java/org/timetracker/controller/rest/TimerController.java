package org.timetracker.controller.rest;

import java.util.List;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.timetracker.service.TimerService;
import org.timetracker.service.dto.CreateTimerDto;
import org.timetracker.service.dto.RecordDto;
import org.timetracker.service.dto.TimerDetailsDto;
import org.timetracker.service.dto.UpdateTimerDto;

@RestController
@RequestMapping("/timers")
public class TimerController {

	private TimerService service;

	public TimerController(TimerService service) {
		this.service = service;
	}

	@GetMapping
	public List<TimerDetailsDto> getTimers() {
		return service.getTimers();
	}

	@GetMapping("/{id}")
	public ResponseEntity<TimerDetailsDto> getTimer(@PathVariable("id") long id) {
		return ResponseEntity.of(service.getTimer(id));
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public TimerDetailsDto createTimer(@RequestBody CreateTimerDto dto) {
		return service.createTimer(dto);
	}

	@PatchMapping("/{id}")
	public ResponseEntity<?> updateTimer(
			@PathVariable("id") long id, @RequestBody UpdateTimerDto dto) {

		try {
			return ResponseEntity.of(service.updateTimer(id, dto));
		} catch(IllegalStateException ex) {
			return ResponseEntity.badRequest().body(ex.getMessage());
		}
	}

	@GetMapping("/{timerId}/records/recent")
	public ResponseEntity<List<RecordDto>> getRecentRecords(@PathVariable("timerId") long timerId) {
		return ResponseEntity.of(service.getRecentRecordsFor(timerId));
	}

	@GetMapping("/{id}/records")
	public Optional<List<RecordDto>> getRecords(@PathVariable("id") long id) {
		return service.getRecordsFor(id);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Long> deleteTimer(@PathVariable("id") long id) {
		return ResponseEntity.of(service.delete(id));
	}
}
