package org.timetracker.service.dto;

import java.time.Instant;
import java.util.List;

public record TimerDetailsDto(
	long id,
	String title,
	long duration,
	Instant startedTime,
	Instant stoppedTime,
	List<RecordDto> records
) {}
