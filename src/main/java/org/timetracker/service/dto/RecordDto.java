package org.timetracker.service.dto;

import java.time.Instant;

public record RecordDto(long id, Instant startedTime, Instant stoppedTime) {}
