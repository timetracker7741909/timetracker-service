package org.timetracker.service.dto;

import java.time.Instant;

public record UpdateTimerDto(String title, Instant startedTime, Instant stoppedTime) {}
