package org.timetracker.service.dto;

public record CreateTimerDto(String title) {}
