package org.timetracker.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.springframework.stereotype.Service;
import org.timetracker.domain.Timer;
import org.timetracker.domain.TimerRecord;
import org.timetracker.persistence.TimerRepository;
import org.timetracker.service.dto.CreateTimerDto;
import org.timetracker.service.dto.RecordDto;
import org.timetracker.service.dto.TimerDetailsDto;
import org.timetracker.service.dto.UpdateTimerDto;
import org.timetracker.service.exception.NoSuchTimerException;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class TimerService {

	private final TimerRepository repo;

	@Transactional
	public TimerDetailsDto createTimer(CreateTimerDto dto) {
		return toDto(repo.save(new Timer(dto.title())));
	}

	public Optional<TimerDetailsDto> getTimer(long id) {
		return repo.findById(id)
			.map(this::toDto);
	}

	public List<TimerDetailsDto> getTimers() {
		return repo.findAll()
			.stream()
			.map(this::toDto)
			.toList();
	}

	public Optional<TimerDetailsDto> updateTimer(long id, UpdateTimerDto dto) {
		return repo.findById(id)
			.map(t -> applyUpdate(t, dto))
			.map(repo::save)
			.map(this::toDto);
	}

	public Optional<Long> delete(long id) {
		return repo.findById(id)
			.map(t -> {
				repo.delete(t);
				return t;
			})
			.map(t -> t.getId());
	}

	public Optional<List<RecordDto>> getRecordsFor(long tid) {
		return repo.findById(tid)
			.map(t -> t.getRecords()
					.stream()
					.map(this::toDto)
					.toList());
	}

	public Optional<List<RecordDto>> getRecentRecordsFor(long tid) {
		return repo.findById(tid)
			.map(t -> streamRecentRecords(t).map(this::toDto).toList());
	}

	private Timer applyUpdate(Timer t, UpdateTimerDto dto) {
		if(dto.title() != null) t.setTitle(dto.title());

		if(dto.startedTime() != null) {
			t.start(dto.startedTime());
		} else if(dto.stoppedTime() != null) {
			t.stop(dto.stoppedTime());
		}

		return t;
	}

	private Stream<TimerRecord> streamRecentRecords(Timer t) {
		return t.getRecords().stream()
			.limit(5);
	}

	private RecordDto toDto(TimerRecord r) {
		return new RecordDto(r.getId(), r.getStartedTime(), r.getStoppedTime());
	}

	private TimerDetailsDto toDto(Timer t) {
		return new TimerDetailsDto(
				t.getId(),
				t.getTitle(),
				t.getDuration().toSeconds(),
				t.getStartedTime(),
				t.getStoppedTime(),
				t.getRecords().stream().map(this::toDto).toList());
	}
}
