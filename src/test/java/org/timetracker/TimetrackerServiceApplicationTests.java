package org.timetracker;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@Disabled
@SpringBootTest
class TimetrackerServiceApplicationTests {

	@Test
	void contextLoads() {
	}

}
