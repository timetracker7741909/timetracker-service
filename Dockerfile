FROM gradle:7.6.0-jdk19-alpine AS build
WORKDIR /home/gradle/timetracker
COPY --chown=gradle:gradle . /home/gradle/timetracker
RUN gradle bootJar --no-daemon

FROM eclipse-temurin:19-jre-alpine
WORKDIR /opt/timetracker
COPY --from=build /home/gradle/timetracker/build/libs/*.jar timetracker.jar
ENTRYPOINT [ "java", "-jar", "./timetracker.jar" ]
